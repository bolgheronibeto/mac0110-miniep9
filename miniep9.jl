using Test
using LinearAlgebra
# aux
function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function nth_identity(n)
    # m = zeros(Int64, n, n)
    # for i = 1:n
    #     m[i, i] = 1
    # end
    # return m
    return Matrix(LinearAlgebra.I, n, n)
end

# pt.1

function matrix_pot(M, p)
    resultante = copy(M)
    i = 1
    while i < p
        resultante = multiplica(M, resultante)
        i += 1
    end
    return resultante
end

# pt.2

# p £ Z | p >= 1
function matrix_pot_by_squaring(M, p)
    if p == 0
        return nth_identity(size(M)[0])
    end
    if p == 1
        return M
    end
    square = multiplica(M, M)
    if p % 2 == 0  
        # p is even
        return matrix_pot_by_squaring(square, p / 2)
    else
        # p is odd
        return multiplica(M, matrix_pot_by_squaring(square, ((p - 1) / 2)))
    end
end