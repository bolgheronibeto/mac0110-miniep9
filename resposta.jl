include("miniep9.jl")
using LinearAlgebra

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)

    println("2pow")
    @time matrix_pot(M, 2)
    @time matrix_pot_by_squaring(M, 2)

    println("5pow")
    @time matrix_pot(M, 5)
    @time matrix_pot_by_squaring(M, 5)

    println("10pow")
    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)

    println("15pow")
    @time matrix_pot(M, 15)
    @time matrix_pot_by_squaring(M, 15)
    
    println("20pow")
    @time matrix_pot(M, 20)
    @time matrix_pot_by_squaring(M, 20)

    println("30pow")
    @time matrix_pot(M, 20)
    @time matrix_pot_by_squaring(M, 20)

end

compare_times()

# Houve diferenca nos tempos de execucao?  Por quê?
# resposta: Houve, sim, diferenca e, quanto maiores as quantidades de calculos, maior ela eh. Chega-se ao ponto de 
# a by_squaring ser ate 5 vezes mais rapida que a matrix_pot inicial.
# Isso se da pela forma como escalam a complexidade. Enquanto a matrix_pot inicial
# cresce exponencialmente, realizando p multiplicacoes de matrizes, 
# a funcao matrix_pot_by_squaring realiza muito menos, evitando varios calculos